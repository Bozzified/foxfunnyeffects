var Button = Button || {};

(function(win) {

    Button = function(el) {
        this.container = el;
        this.flipper = $('.inner', el);
        this.frontface = $('.btn-front', this.flipper);
        this.backface = $('.btn-back', this.flipper);
        var tl = TweenMax;

        console.log(this.container.height()/2);
        tl.set(this.container, {transformPerspective: 1400, transformStyle: "preserve-3d"});
        tl.set(this.flipper, {transformStyle: "preserve-3d"});
        tl.set(this.backface, {transform: "rotateX(-89deg) translateZ("+this.container.height()/2+"px)"});
        tl.set(this.frontface, {transform: "translateZ("+this.container.height()/2+"px)"});

        var self = this;

        this.flipper.hover(function() {
            self.rollOver();
        }, function() {
            self.rollOut();
        })

        return this.container;
    };


    Button.prototype.rollOver = function() {
        TweenLite.to(this.flipper, 0.5, {
            transform: "rotateX(89deg)",
            transformOrigin: "center center",
            ease: Back.easeInOut
        });
    }

    Button.prototype.rollOut = function() {
        TweenLite.to(this.flipper, 0.5, {
            transform: "rotateX(0deg)",
            transformOrigin: "center center",
            ease: Back.easeInOut
        });
    }

}(window));