// namespace
var Cards = Cards || {};

(function (win) {

    "use strict";

    var el = $('.cards-container');
    var coord = {};

    Cards = function () {
        var _self = this;
        var scaleProportion = 1;
        var offsetSizeY = -3;
        var offsetSizeX = 8;

        return {
            calculate: function () {
                console.log("calculate position coordinates for cards...");
                this.matchMediaParams();
                coord = {
                    offsetPercY: App.deviceSettings.isMobile == true ? 0 : offsetSizeY,
                    offsetPercX: App.deviceSettings.isMobile == true ? 2 : offsetSizeX,
                    x: 0,
                    y: 0, //(App.winH / 2) - (el.height() / 2) - ((coord.offsetPercY * App.winH) / 100),
                    z: 0,
                    scale: App.deviceSettings.isMobile == true ? 0.5 : scaleProportion
                };
            },
            resize: function (w, h) {
                console.log('adjust cards container size')
                var elW = 2400;
                var offsetX = elW / 2;
                // center the cards container horizontally
                $(el).width(elW);
                TweenMax.set(el, {xPercent: -50, yPercent: -50});
                $(el).css('height', '2000px');
                this.calculate();
                this.reposition();
            },
            init: function (cb) {
                console.log('initialize the cards container');
                TweenLite.set(el, {transformPerspective: 2400, transformStyle: "preserve-3d"});
                this.calculate();
                this.renderCards(App.containerEl, function (el) {
                    //TweenMax.set(el, {alpha: 0.9});
                    //TweenMax.to(el, 2, {alpha: 1});
                    console.log("cards rendered...");
                    if (typeof cb === "function") {
                        cb();
                    }
                });
            },
            reposition: function () {
                console.log(coord);
                console.log("reposition cards...");
                TweenMax.set(el, {xPercent: -50 - coord.offsetPercX, yPercent: -50 - coord.offsetPercY});
                TweenMax.set(el, {
                    scale: coord.scale,
                    rotationY: -35, // -30
                    rotationZ: 20, // 30
                    rotationX: 55, // 20
                    transformOrigin: "top 50%"
                })
            },
            renderCards: function (targetEl, cb) {
                console.log("rendering cards");
                var queueArr = App.queueData.getItems(true);
                // add the cards
                for (var i = 0; i < App.cardCount; i++) {
                    var f1, f2, filename1, filename2;

                    f1=Math.floor(Math.random() * (App.cardCount + 1));
                    f2=Math.floor(Math.random() * (App.cardCount + 1));

                    if(i === 27) {
                        //filename1 = "../images/perspective/main.jpg";
                        //filename2 = "../images/perspective/main.jpg";
                        f1 = queueArr.length-1;
                        f2 = queueArr.length-1;
                    } else {
                        //console.log(f1);
                        //console.log(f2);
                        //console.log(queueArr);
                        //console.log(queueArr[f1].result.currentSrc);
                        //console.log(queueArr[f2].result.currentSrc);
                        //console.log(queueArr[f1].result);
                        //console.log(queueArr[f2].result);
                        //
                        //filename1 = queueArr[f1].result.currentSrc;
                        //filename2 = queueArr[f2].result.currentSrc;

                    }
                    var card = new Card(f1, f2, App.containerEl);
                    card.addChild();
                }
                this.startFlipping();
                if (typeof cb === "function") {
                    cb(targetEl);
                }
            },
            startFlipping: function() {

                var flipTimer1 = setInterval(flipCard, 500);
                var flipTimer2 = setInterval(flipCard, 1000);
                var flipTimer3 = setInterval(flipCard, 2000);
                var flipTimer4 = setInterval(flipCard, 3000);

                function flipCard() {
                    var rn = Math.floor(TweenMax.randomNumber(0, App.cardCount-1));
                    var card = $('.card').eq(rn);
                    card[0].flip3d();
                }
            },
            matchMediaParams: function() {
                if (matchMedia('only screen and (max-height: 600px)').matches) {
                    console.log("600");
                    scaleProportion = 0.6;
                    offsetSizeY = -4;
                    offsetSizeX = 5;
                }
                if (matchMedia('only screen and (min-height: 601px) and (max-height: 680px)').matches) {
                    console.log("680");
                    scaleProportion = 1;
                    offsetSizeY = -3;
                    offsetSizeX = 8;
                }
                if (matchMedia('only screen and (min-height: 681px) and (max-height: 880px)').matches) {
                    scaleProportion = 1.1;
                    offsetSizeY = -2;
                    offsetSizeX = 9;
                }
                if (matchMedia('only screen and (min-height: 881px)').matches) {
                    scaleProportion = 1.3;
                    offsetSizeY = 0;
                    offsetSizeX = 10;
                }
            }
        };
    };
}(window));