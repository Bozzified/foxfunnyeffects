// namespace
var DisplayData = DisplayData || {};

(function (win) {

    "use strict";

    DisplayData = {
            domFragments: null,
            loadFragments: function (fragmentFile) {
                // loading display objects for use with code
                $.get(fragmentFile, function (response, status, xhr) {
                    if (status === "error") {
                        console.log("Sorry but there was an error: " + xhr.status + " " + xhr.statusText);
                    }
                    if (status === "success") {
                        DisplayData.domFragments=response;
                        console.log("--- success loading fragments ---");
                    }

                }).then(function () {
                    App.isDataLoaded = true;
                    $(App).trigger("dataAssetsLoaded");
                });
            }
    };


}(window));
