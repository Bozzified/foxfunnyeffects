// namespace
var Card = Card || {};

(function (win) {

    "use strict";
    var cardOpacity = 0.2;
    Card = function (frontImage, backImage, ctx) {
        var highlightedCard = 27;
        var fImg = frontImage;
        var bImg = backImage;
        var fragments = $(DisplayData.domFragments)[0];
        var cardFragment = $(fragments).find('.card');
        var front = $(cardFragment).find('.front');
        var back = $(cardFragment).find('.back');
        var left = $(cardFragment).find('.sideleft');
        var right = $(cardFragment).find('.sideright');
        var bottom = $(cardFragment).find('.sidebottom');
        var isFlipped = false;
        var _self = this;
        return {
            addChild: function () {
                ctx.append(cardFragment[0]);

                // assign background images to front/back of the visual presentation
                // handling raw blob image generation
                var images = App.queueData.getItems(true);
                var fBlob = images[fImg].rawResult;
                var bBlob = images[bImg].rawResult;
                var fImageUrl = win.URL.createObjectURL( fBlob );
                var bImageUrl = win.URL.createObjectURL( bBlob );
                front.find('img').attr('src', fImageUrl);
                back.find('img').attr('src', bImageUrl);

                var tl = TweenMax;
                tl.set([back, front, left, right], {backfaceVisibility: "hidden"});
                tl.set([bottom], {backfaceVisibility: "visible"});
                tl.set([cardFragment], {transformStyle: "preserve-3d"});
                tl.set($('img', front), {alpha: cardOpacity, xPercent: -50, yPercent: -50});
                tl.set($('img', back), {alpha: 0, width: "100%", height: "100%", xPercent: -50, yPercent: -50});

                tl.set(front, {transform: "translateZ(100px)"});
                tl.set(back, {transform: "rotateX(-90deg) translateZ(100px)"});
                tl.set(left, {transform: "rotateY(-90deg) translateZ(100px)"});
                tl.set(right, {transform: "rotateY(-270deg) translateZ(280px)"});
                tl.set(bottom, {transform: "translateZ(-100px)"});
                //tl.set(cardFragment, {scale: 1, alpha: 1});
                cardFragment[0].flip3d = this.flip3d;
                cardFragment[0].addChild = this.addChild;
                cardFragment[0].idN = $(cardFragment).index();

                if(cardFragment[0].idN !== highlightedCard) {
                    TweenLite.fromTo($('img', front), 0.5, {
                            //alpha: 0,
                            width: "100%",
                            height: "100%",
                            borderRadius: "0%"},
                        {
                            //alpha: 0.2,
                            width: "100%",
                            height: "100%",
                            borderRadius: "0%",
                            delay: TweenMax.randomNumber(0,1)+1
                        });
                } else {
                    TweenLite.fromTo($('img', front), 0.5, {
                            alpha: 0,
                            width: "100%",
                            height: "100%",
                            borderRadius: "0%"},
                        {
                            alpha: 1,
                            width: "100%",
                            height: "100%",
                            borderRadius: "0%",
                            delay: 0
                        });
                }


                if(cardFragment[0].idN === highlightedCard) {
                    TweenLite.to(cardFragment, 0.5, {
                        transform: "rotateX(0deg) translateZ(40px)",
                        transformOrigin: "center center",
                        delay: 2,
                    });
                }

                //cardFragment.click(function (e) {
                //    this.flip3d();
                //})
            },
            flip3d: function () {
                var normalTransform = "rotateX(0deg) translateY(0px)";
                var flipTransform = "rotateX(90deg) translateY(0px)";

                if (isFlipped) {
                    if(this.idN === highlightedCard) {
                        TweenLite.to($('.back', this).find('img'), 0.5, {alpha: 0, scale: 1, ease: Back.easeInOut});
                    } else {
                        TweenLite.to($('.back', this).find('img'), 0.5, {alpha: 0, scale: 1, ease: Back.easeInOut});
                        TweenLite.to(cardFragment, 0.5, {
                            transform: normalTransform,
                            transformOrigin: "center center",
                            ease: Back.easeInOut, onComplete: function () {
                                isFlipped = false;
                            }
                        });
                    }

                } else {
                    if(this.idN === highlightedCard) {
                        TweenLite.to($('.back', this).find('img'), 0.5, {alpha: 0, scale: 1, ease: Back.easeInOut});
                    } else {
                        TweenLite.to($('.back', this).find('img'), 0.5, {alpha: cardOpacity, scale: 1, ease: Back.easeInOut});
                        TweenLite.to(cardFragment, 0.5, {
                            transform: flipTransform,
                            transformOrigin: "center center",
                            ease: Back.easeInOut, onComplete: function () {
                                isFlipped = true;
                            }
                        });
                    }
                }

            },
        }
    }

}(window));