// namespace
var App = App || {};

(function (w) {

    "use strict";

    // TweenMax extends

    TweenMax.randomNumber = function (min, max) {
        return Math.random() * (max - min + 1) + min;
    }
    TweenMax.setCenter = function (el, params) {
        var obj = params;
        obj = typeof obj !== 'undefined' ? obj : {};
        obj.percX = typeof obj.percX !== 'undefined' ? obj.percX : -50;
        obj.percY = typeof obj.percY !== 'undefined' ? obj.percY : -50;

        return TweenMax.set(el, {xPercent: obj.percX, yPercent: obj.percY});
    }
    // end TweenMax extends

    var headerLogoY = 0;
    var registerStep = 1;

    App = {
        currentHashValue: null,
        getCurrentHashValue: function(){ return App.currentHashValue.match(/^#?(.*)$/)[1]},
        getHashValue: function () {
            return location.hash.match(/^#?(.*)$/)[1]
        },
        setHashValue: function (value) {
            App.currentHashValue = '#' + value;
            window.location.hash = App.currentHashValue;
        },
        isDataLoaded: false,
        cardCount: 48,
        containerEl: $('.cards-container'),
        mainContainer: $('.container'),
        ui: {
            suptitle: $('.home-suptitle'),
            logo: $('.home-logo'),
            desc: $('.home-description'),
            notice: $('.home-coming'),
            loginBtn: $('.login-bttn'),
            learnBtn: $('.learn-more-bttn'),
            uploadBtn: $('.upload-video-bttn'),
            footer: $('footer'),
            contentC: $('.content-container'),
            regContainer: $('.registration-container')
        },
        winW: $(w).innerWidth(),
        winH: $(w).innerHeight(),
        cards: new Cards(),
        deviceSettings: {},
        queueData: null,
        preloader: $('.preloader'),
        setupDeviceSettings: function () {
            var e = navigator.userAgent.toLowerCase();
            App.deviceSettings.isAndroid = e.indexOf("android") > -1;
            App.deviceSettings.isiPod = navigator.userAgent.match(/iPod/i) != null;
            App.deviceSettings.isiPhone = navigator.userAgent.match(/iPhone/i) != null;
            App.deviceSettings.isiPad = navigator.userAgent.match(/iPad/i) != null;
            App.deviceSettings.isiOS = navigator.userAgent.match(/(iPad|iPhone|iPod)/i) ? true : false;
            var t = navigator.platform.toLowerCase();
            if (App.deviceSettings.isAndroid || App.deviceSettings.isiPad || t === "ipad" || t === "iphone" || t === "ipod" || t === "android" || t === "palm" || t === "windows phone" || t === "blackberry" || t === "linux armv7l") {
                App.deviceSettings.isMobile = true;
            }
        },
        init: function initStage() {
            console.log("init stage");
            TweenMax.set(App.mainContainer, {xPercent: -50, yPercent: -50});
            App.mainContainer.width(30);
            App.mainContainer.height(30);
            App.setupDeviceSettings();
            // $(".nano").nanoScroller({ sliderMaxHeight: 50, alwaysVisible: true, disableResize: !1 });

            $("#video-upload").dropzone({
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 2, // MB
                thumbnailWidth: 300,
                thumbnailHeight: 150,
                accept: function (file, done) {
                    console.log("uploaded");
                    done();
                },
                init: function () {
                    this.on("addedfile", function () {
                        if (this.files[1] != null) {
                            this.removeFile(this.files[0]);
                        }
                    });
                }
            });

            App.cards.init(function () {
                //App.mainContainer.show();
                App.showStage();
            });

        },
        resize: function () {
            // refresh the window width/height on resize
            App.winW = $(w).innerWidth();
            App.winH = $(w).innerHeight();
            App.mainContainer.width(App.winW);
            App.mainContainer.height(App.winH);
            //App.ui.contentC.width(App.winW);
            // adjust sizes of the container
            App.cards.resize(App.winW, App.winH);
            $('.registration-container').height(App.winH - 200);
            $('.content-holder').height(App.winH - 300);
            $('.content-holder').perfectScrollbar("update");
            //App.ui.regContainer.height(App.ui.contentC.height() - 290 - App.ui.footer.height());
            //App.ui.regContainer.height(App.ui.contentC.height() - 290);
        },
        showStage: function () {
            console.log("appear");
            App.mainContainer.show();
            App.cards.resize(App.winW, App.winH);
            TweenMax.to(App.mainContainer, 0.5, {
                borderRadius: "0%",
                width: App.winW,
                height: App.winH,
                ease: Back.easeIn,
                delay: 0.3,
                onComplete: function () {

                    $('.content-holder').perfectScrollbar({
                        wheelPropagation: false,
                        maxScrollbarLength: 40,
                        suppressScrollX: true
                    });
                    App.initUI();
                }
            });
            w.onresize = App.resize;
        },
        preloadAssets: function () {

            var queue = new createjs.LoadQueue(true);
            var queueArray = [];
            for (var i = 0; i < App.cardCount; i++) {
                queueArray.push("images/perspective/front" + i + ".jpg");
            }
            queueArray.push("images/perspective/main.jpg");

            queue.loadManifest(queueArray);
            queue.on("complete", handleComplete, this);

            function handleComplete(event) {
                console.log("preload finished..");
                App.queueData = event.target;
                TweenMax.to($('svg', App.preloader), 1, {
                    alpha: 0, onComplete: function () {
                        TweenMax.to(App.preloader, 0.5, {
                            width: 30,
                            height: 30,
                            ease: Back.easeIn,
                            onComplete: function () {
                            }
                        });
                        App.init();
                    }
                });
            }
        },
        initUI: function () {
            TweenMax.set(App.ui.suptitle, {yPercent: -50, xPercent: -50});
            TweenMax.set(App.ui.logo, {yPercent: -50, xPercent: -50});
            TweenMax.set($('p', App.ui.notice), {transformPerspective: 200, transformOrigin: "top center"});
            TweenMax.set($('p', App.ui.notice), {transform: "rotateX(-90deg)"});
            TweenMax.set($('.description'), {xPercent: -50});
            TweenMax.set(App.ui.desc, {xPercent: -50, yPercent: -50, left: "50%"});
            TweenMax.set(App.ui.learnBtn, {xPercent: -50, left: "50%"});
            TweenMax.set(App.ui.uploadBtn, {xPercent: -50, left: "50%"});

            var loginBtn = new Button(App.ui.loginBtn);
            var loginBtnX = loginBtn.css('right');
            loginBtn.show();

            var uploadBtn = new Button(App.ui.uploadBtn);
            uploadBtn.css('display', 'inline-block');

            var createAccountBtn = new Button($('.create-bttn'));
            createAccountBtn.css('display', 'inline-block');

            var submitUploadBtn = new Button($('.uploadsubmit-btn'));
            var browseBtn = new Button($('.browse-bttn'));

            var viewVideosBtn = new Button($('.viewvideos-bttn'));
            var uploadNewVideoBtn = new Button($('.uploadnewvideo-bttn'));

            TweenMax.fromTo(loginBtn, 0.5, {css: {right: "-105px"}}, {
                css: {right: loginBtnX},
                ease: Back.easeOut,
                delay: 2
            });

            App.ui.suptitle.show();
            App.ui.logo.show();
            App.ui.notice.show();
            App.ui.desc.show();
            App.ui.learnBtn.show();
            App.ui.footer.show();

            uploadBtn.click(function () {
                App.showRegistration();
                App.setHashValue('register');
            });
            App.ui.logo.click(function () {
                App.setHashValue('home');
            });
            createAccountBtn.click(function () {
                App.showUpload();
                //App.setHashValue('upload');
            });

            submitUploadBtn.click(function () {
                App.showShare();
                //App.setHashValue('share');
            });

            $('.uploadnewvideo-bttn').click(function() {
            });

            registerStep = 1;

            TweenMax.fromTo(App.ui.suptitle, 0.5, {scale: 0, alpha: 0, ease: Back.easeInOut}, {
                scale: 1,
                alpha: 1,
                ease: Back.easeInOut
            });
            TweenMax.fromTo(App.ui.logo, 0.5, {scale: 0, alpha: 0, ease: Back.easeInOut}, {
                scale: 1,
                alpha: 1,
                ease: Back.easeInOut,
                delay: 1
            });
            TweenMax.to($('p', App.ui.notice), 0.5, {
                transform: "rotateX(0deg)",
                ease: Back.easeOut,
                delay: 1.8
            });

            TweenMax.fromTo(App.ui.desc, 1, {scale: 0, alpha: 0}, {
                scale: 1,
                alpha: 1,
                ease: Back.easeInOut,
                delay: 1.4
            });
            TweenMax.fromTo(App.ui.learnBtn, 1, {scale: 0, alpha: 0}, {
                scale: 1,
                alpha: 1,
                ease: Back.easeInOut,
                delay: 1.6
            });
            TweenMax.fromTo(App.ui.uploadBtn, 1, {scale: 0, alpha: 0}, {
                scale: 1,
                alpha: 1,
                ease: Back.easeInOut,
                delay: 1.8
            });
            TweenMax.fromTo(App.ui.footer, 1, {alpha: 0}, {alpha: 1, ease: Back.easeInOut, delay: 1.5});
        },
        showRegister: function () {
            if (registerStep === 2) {
                TweenMax.to($('.upload'), .5, {
                    left: "-100%", ease: Expo.easeInOut, onComplete: function () {
                        $('.upload').css('left', '100%');
                    }
                });
            }
            if (registerStep === 3) {
                TweenMax.to($('.share'), .5, {
                    left: "-100%", ease: Expo.easeInOut, onComplete: function () {
                        $('.upload').css('left', '100%');
                    }
                });
            }
            registerStep = 1;
            TweenMax.to($('.signup'), .5, {left: "0", ease: Expo.easeInOut});
        },
        showUpload: function () {
            App.showUploadUI();
            $('.upload').css('left', '100%');
            TweenMax.to($('.signup'), .5, {
                left: "-100%", ease: Expo.easeInOut, onComplete: function () {
                    $('.signup').css('left', '100%');
                }
            });
            TweenMax.to($('.upload'), .5, {
                left: "0", ease: Expo.easeInOut, onStart: function () {
                    $('.upload').show()
                }
            });
            TweenMax.to($('.step-circle').eq(1).find('.step-number'), 0.5, {
                scale: 1.3,
                ease: Back.easeInOut,
                delay: 0,
                onComplete: function () {
                    $('.step-circle').eq(1).find('.step-number').addClass('step-active');
                }
            });
            registerStep = 2;
        },
        showUploadUI: function () {
            App.resetSteps();
            //TweenMax.to(App.containerEl, 1, {scale:0.8, rotation: 10});
            TweenMax.fromTo($('.upload .register-title'), 0.5, {alpha: 0, y: "+=25"}, {
                alpha: 1,
                y: 0,
                ease: Back.easeInOut,
                delay: 0.5
            });
            TweenMax.fromTo($('.upload .upload-target'), 0.5, {alpha: 0, y: "+=25"}, {
                alpha: 1,
                y: 0,
                ease: Back.easeInOut,
                delay: 0.6
            });
            TweenMax.fromTo($('.upload .how-it-works h1'), 0.5, {alpha: 0, y: "+=25"}, {
                alpha: 1,
                y: 0,
                ease: Back.easeInOut,
                delay: 0.7
            });
            TweenMax.fromTo($('.upload .how-it-works p'), 0.5, {alpha: 0, y: "+=25"}, {
                alpha: 1,
                y: 0,
                ease: Back.easeInOut,
                delay: 0.8
            });
            TweenMax.fromTo($('.upload .video-req h1'), 0.5, {alpha: 0, y: "+=25"}, {
                alpha: 1,
                y: 0,
                ease: Back.easeInOut,
                delay: 0.9
            });
            TweenMax.fromTo($('.upload .video-req p'), 0.5, {alpha: 0, y: "+=25"}, {
                alpha: 1,
                y: 0,
                ease: Back.easeInOut,
                delay: 1.0
            });
            TweenMax.fromTo($('.upload .rightside #video-description .fields').eq(0), 0.5, {alpha: 0, y: "+=25"}, {
                alpha: 1,
                y: 0,
                ease: Back.easeInOut,
                delay: 1.1
            });
            TweenMax.fromTo($('.upload .rightside #video-description .fields').eq(1), 0.5, {alpha: 0, y: "+=25"}, {
                alpha: 1,
                y: 0,
                ease: Back.easeInOut,
                delay: 1.2
            });
            TweenMax.fromTo($('.upload .rightside #video-description .fields').eq(2), 0.5, {alpha: 0, y: "+=25"}, {
                alpha: 1,
                y: 0,
                ease: Back.easeInOut,
                delay: 1.3
            });
            TweenMax.fromTo($('.upload .rightside #video-description .fields').eq(3), 0.5, {alpha: 0, y: "+=25"}, {
                alpha: 1,
                y: 0,
                ease: Back.easeInOut,
                delay: 1.4
            });
            TweenMax.fromTo($('.upload .uploadsubmit-btn'), 0.5, {alpha: 0, y: "+=25"}, {
                alpha: 1,
                y: 0,
                ease: Back.easeInOut,
                delay: 1.5
            });
        },
        showShareUI: function () {
            App.resetSteps();

            //TweenMax.to(App.containerEl, 1, {scale:1, rotation: 0});
            TweenMax.fromTo($('.share .register-title'), 0.5, {alpha: 0, y: "+=25"}, {
                alpha: 1,
                y: 0,
                ease: Back.easeInOut,
                delay: 0.5
            });
            TweenMax.fromTo($('.share .register-desc'), 0.5, {alpha: 0, y: "+=25"}, {
                alpha: 1,
                y: 0,
                ease: Back.easeInOut,
                delay: 0.6
            });
            TweenMax.fromTo($('.share .upload-preview'), 0.5, {alpha: 0, y: "+=25"}, {
                alpha: 1,
                y: 0,
                ease: Back.easeInOut,
                delay: 0.7
            });
            TweenMax.fromTo($('.share .socialshare'), 0.5, {alpha: 0, y: "+=25"}, {
                alpha: 1,
                y: 0,
                ease: Back.easeInOut,
                delay: 0.8
            });
            TweenMax.fromTo($('.share .successmsg'), 0.5, {alpha: 0, y: "+=25"}, {
                alpha: 1,
                y: 0,
                ease: Back.easeInOut,
                delay: 0.9
            });
            TweenMax.fromTo($('.share .buttonactions'), 0.5, {alpha: 0, y: "+=25"}, {
                alpha: 1,
                y: 0,
                ease: Back.easeInOut,
                delay: 1.0
            });

        },
        showShare: function () {
            App.showShareUI();
            $('.share').css('left', '100%');
            TweenMax.to($('.upload'), .5, {
                left: "-100%", ease: Expo.easeInOut, onComplete: function () {
                    $('.upload').css('left', '100%');
                }
            });
            TweenMax.to($('.share'), .5, {
                left: "0", ease: Expo.easeInOut, onStart: function () {
                    $('.share').show()
                }
            });
            TweenMax.to($('.step-circle').eq(2).find('.step-number'), 0.5, {
                scale: 1.3,
                ease: Back.easeInOut,
                delay: 0,
                onComplete: function () {
                    $('.step-circle').eq(2).find('.step-number').addClass('step-active');
                }
            });
            registerStep = 3;
        },
        showRegistration: function () {
            headerLogoY = $('.intro-top').css("top");

            TweenMax.set($('.registration-container'), {top: App.winH + 100 + "px"});
            TweenMax.to($('.registration-container'), 0.5, {top: "200px", ease: Expo.easeInOut});
            TweenMax.to($('.intro-top'), 0.5, {scale: 0.7, top: "80px", ease: Back.easeInOut});

            TweenMax.fromTo($('.step-circle').eq(0), 1, {alpha: 0, scale: 0}, {
                alpha: 1,
                scale: 1,
                ease: Back.easeInOut,
                delay: 0.2
            });
            TweenMax.fromTo($('.step-circle').eq(1), 1, {alpha: 0, scale: 0}, {
                alpha: 1,
                scale: 1,
                ease: Back.easeInOut,
                delay: 0.4
            });
            TweenMax.fromTo($('.step-circle').eq(2), 1, {alpha: 0, scale: 0}, {
                alpha: 1,
                scale: 1,
                ease: Back.easeInOut,
                delay: 0.6
            });

            TweenMax.to($('.step-circle').eq(0).find('.step-number'), 0.5, {
                scale: 1.3,
                ease: Back.easeInOut,
                delay: 1.6,
                onComplete: function () {
                    $('.step-circle').eq(0).find('.step-number').addClass('step-active');
                }
            });

            TweenMax.to($('.intro-bottom'), 0.5, {
                alpha: 0, ease: Expo.easeOut, onComplete: function () {
                    $('.intro-bottom').hide();
                }
            });

            $('.content-holder').height(App.winH - 300);
            $('.content-holder').perfectScrollbar("update");

            //TweenMax.to(App.containerEl, 1, {scale:0.8, rotation: -5});
            App.ui.contentC.show();
            // App.showCreateForm();
        },
        showCreateForm: function () {
            App.showRegister();
            TweenMax.fromTo($('.register-title'), 0.5, {alpha: 0, y: "+=25"}, {
                alpha: 1,
                y: 0,
                ease: Back.easeInOut,
                delay: 0.5
            });
            TweenMax.fromTo($('.register-desc'), 0.5, {alpha: 0, y: "+=25"}, {
                alpha: 1,
                y: 0,
                ease: Back.easeInOut,
                delay: 0.6
            });
            TweenMax.fromTo($('#createaccount .fields').eq(0), 0.5, {alpha: 0, y: "+=25"}, {
                alpha: 1,
                y: 0,
                ease: Back.easeInOut,
                delay: 0.7
            });
            TweenMax.fromTo($('#createaccount .fields').eq(1), 0.5, {alpha: 0, y: "+=25"}, {
                alpha: 1,
                y: 0,
                ease: Back.easeInOut,
                delay: 0.8
            });
            TweenMax.fromTo($('#createaccount .fields').eq(2), 0.5, {alpha: 0, y: "+=25"}, {
                alpha: 1,
                y: 0,
                ease: Back.easeInOut,
                delay: 0.9
            });
            TweenMax.fromTo($('#createaccount .fields').eq(3), 0.5, {alpha: 0, y: "+=25"}, {
                alpha: 1,
                y: 0,
                ease: Back.easeInOut,
                delay: 1.0
            });
            TweenMax.fromTo($('.create-bttn'), 0.5, {alpha: 0, y: "+=25"}, {
                alpha: 1,
                y: 0,
                ease: Back.easeInOut,
                delay: 1.1
            });
            TweenMax.fromTo($('.login-footer'), 0.5, {alpha: 0, y: "+=25"}, {
                alpha: 1,
                y: 0,
                ease: Back.easeInOut,
                delay: 1.2
            });
        },
        hideRegistration: function () {
            registerStep = 1;
            $('.registration-container').height(App.winH - 200);
            TweenMax.to($('.registration-container'), 0.5, {top: App.winH + 100 + "px", ease: Expo.easeInOut});
            TweenMax.to($('.intro-top'), 0.5, {scale: 1, top: headerLogoY, ease: Back.easeInOut});
            $('.intro-bottom').show();
            TweenMax.to($('.intro-bottom'), 0.5, {alpha: 1, ease: Expo.easeOut, onComplete: function() {
                $('.signup').css('left', '100%');
                $('.upload').css('left', '100%');
                $('.share').css('left', '100%');
            }});

        },
        resetSteps: function () {

            TweenMax.to($('.step-circle').eq(0).find('.step-number'), 0.5, {
                scale: 1,
                ease: Back.easeInOut,
                delay: 0,
                onComplete: function () {
                    $('.step-circle').eq(0).find('.step-number').removeClass('step-active');
                }
            });
            TweenMax.to($('.step-circle').eq(1).find('.step-number'), 0.5, {
                scale: 1,
                ease: Back.easeInOut,
                delay: 0,
                onComplete: function () {
                    $('.step-circle').eq(1).find('.step-number').removeClass('step-active');
                }
            });
            TweenMax.to($('.step-circle').eq(2).find('.step-number'), 0.5, {
                scale: 1,
                ease: Back.easeInOut,
                delay: 0,
                onComplete: function () {
                    $('.step-circle').eq(2).find('.step-number').removeClass('step-active');
                }
            });
        },
        handleHash: function(hash) {

            if( hash === 'home') {
                App.resetSteps();
                App.hideRegistration();
            }
            if( hash === 'register'){
                console.log("handleHash + " + hash);
                App.resetSteps();
                App.showCreateForm();
            }
            if(hash === 'upload'){
                App.resetSteps();
                App.showUpload();
            }
            if(hash === 'share'){
                App.resetSteps();
                App.showShare();
            }
        }

    }

    $(document).ready(function () {
        DisplayData.loadFragments("htmlfragments/domFragments.html");
        $(App).on('dataAssetsLoaded', function (obj, data) {
            TweenMax.set(App.preloader, {xPercent: -50, yPercent: -50, x: 0, y: 0});
            App.preloader.show();
            App.preloadAssets();
        });
        App.resize();
        if (Modernizr.webgl) {
            console.log("supports webgl");
        }

        $(window).on('hashchange', function() {
            console.log('trigger hash change', App.getHashValue());
            App.handleHash(App.getHashValue());
        });


    });

}(window));